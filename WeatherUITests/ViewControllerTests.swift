//
//  WeatherUITests.swift
//  WeatherUITests
//
//  Created by Victor Meyer on 2018/03/05.
//  Copyright © 2018 DVT. All rights reserved.
//

import XCTest

class WeatherUITests: XCTestCase
{
	
	// MARK: properties
	var app: XCUIApplication!
	
	// MARK: life cycle
    override func setUp()
	{
        super.setUp()
        
		self.app = XCUIApplication()
		
		continueAfterFailure = false
		self.app.launch()
    }
    
    override func tearDown()
	{
		self.app = nil

        super.tearDown()
    }

	// MARK: refresh button test
    func testRefreshButton()
	{
		// given
		let window = self.app.windows.element(boundBy: 0)
		let loading = self.app.staticTexts["Loading Weather Information"]
		
		// when
		self.app.buttons["Refresh"].tap()
		
		// then
		XCTAssert(window.frame.contains(loading.frame))
    }
    
}
