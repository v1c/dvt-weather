//
//  LocationManager.swift
//  Weather
//
//  Created by Victor Meyer on 2018/03/03.
//  Copyright © 2018 DVT. All rights reserved.
//

import Foundation
import CoreLocation

class LocationService: NSObject
{
	
	// MARK: types
	typealias LocationCompletionHandler = (CLLocationCoordinate2D?, String?) -> Void
	
	// MARK: properties
	var locationManager: CLLocationManager?
	var completionHandler: LocationCompletionHandler?
	var coordinates: CLLocationCoordinate2D?
	var error: String?
	var locationSent: Bool = false
	
	// MARK: setup
	init(manager: CLLocationManager, completion: @escaping LocationCompletionHandler)
	{
		super.init()
		
		// save handler for later
		self.completionHandler = completion
		
		// location manager
		self.locationManager = manager
		self.locationManager?.delegate = self
		self.locationManager?.requestWhenInUseAuthorization()
		
		if (CLLocationManager.locationServicesEnabled())
		{
			self.locationManager?.desiredAccuracy = kCLLocationAccuracyBest
			self.start()
		}
	}
	
	// MARK: start/stop location service
	func start()
	{
		self.locationSent = false
		self.locationManager?.startUpdatingLocation()
	}
	
	func stop()
	{
		self.locationSent = true
		self.locationManager?.stopUpdatingLocation()
	}
	
	// MARK: response
	func locationResult()
	{
		// send only once then stop service
		if (!self.locationSent)
		{
			self.stop()
			self.completionHandler?(self.coordinates, self.error)
		}
	}

}
