//
//  NetworkDelegate.swift
//  Weather
//
//  Created by Victor Meyer on 2018/03/05.
//  Copyright © 2018 DVT. All rights reserved.
//

import Foundation

protocol NetworkDelegate
{

	func fetchRemoteData(url: URL, completion: @escaping (Any?, String?) -> Void)
	
}
