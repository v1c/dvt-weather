//
//  NetworkImageDelegate.swift
//  Weather
//
//  Created by Victor Meyer on 2018/03/06.
//  Copyright © 2018 DVT. All rights reserved.
//

import UIKit

protocol NetworkImageDelegate
{
	
	func fetchRemoteImage(url: URL, completion: @escaping (UIImage) -> Void)
	
}
