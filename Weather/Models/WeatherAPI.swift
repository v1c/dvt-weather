//
//  WeatherAPI.swift
//  Weather
//
//  Created by Victor Meyer on 2018/03/04.
//  Copyright © 2018 DVT. All rights reserved.
//

import Foundation
import JASON

class WeatherAPI
{
	
	// MARK: parse json from open weather map and create forecast
	static func parseJSON(data: Any) -> Forecast?
	{
		var forecast: Forecast?
		let json = JSON(data)
		
		if let name = json["name"].string,
			let country = json["sys"]["country"].string,
			let minTemp = json["main"]["temp_min"].int,
			let maxTemp = json["main"]["temp_max"].int,
			let icon = json["weather"][0]["icon"].string
		{
			forecast = Forecast(date: Date(), location: "\(name), \(country)", maxTemp: maxTemp, minTemp: minTemp, icon: icon)
		}
		
		return forecast
	}
	
}
