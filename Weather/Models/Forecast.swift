//
//  Forecast.swift
//  Weather
//
//  Created by Victor Meyer on 2018/03/04.
//  Copyright © 2018 DVT. All rights reserved.
//

import Foundation

class Forecast
{
	
	// MARK: properties
	var date: Date?
	var location: String?
	var maxTemp: Int?
	var minTemp: Int?
	var icon: String?
	
	// MARK: setup
	init(date: Date, location: String, maxTemp: Int, minTemp: Int, icon: String)
	{
		self.date = date
		self.location = location
		self.maxTemp = maxTemp
		self.minTemp = minTemp
		self.icon = icon
	}
	
}
