//
//  ViewController.swift
//  Weather
//
//  Created by Victor Meyer on 2018/02/28.
//  Copyright © 2018 DVT. All rights reserved.
//

import UIKit
import CoreLocation

public class MainViewController: UIViewController
{
	
	// MARK: IB outlets
	@IBOutlet weak var todaysDate: UILabel!
	@IBOutlet weak var currentLocation: UILabel!
	@IBOutlet weak var maxTemp: UILabel!
	@IBOutlet weak var minTemp: UILabel!
	@IBOutlet weak var weatherIcon: UIImageView!
	@IBOutlet weak var detailsView: UIView!
	
	@IBOutlet weak var activityIndicator: UIActivityIndicatorView!
	@IBOutlet weak var loadingMessage: UILabel!
	@IBOutlet weak var loadingView: UIView!
	
	@IBOutlet weak var refreshbutton: UIButton!
	
	// MARK: properties
	var alert: UIAlertController?
	var locationService: LocationService!
	var networkManager: NetworkManager!
	var networkService: NetworkService!
	var networkImageManager: NetworkImageManager!
	var networkImageService: NetworkImageService!

	// MARK: life cycle
	override public func viewDidLoad()
	{
		super.viewDidLoad()
		
		// determine user location
		self.getUserLocation()
	}

	override public func didReceiveMemoryWarning()
	{
		super.didReceiveMemoryWarning()
	}
	
	// MARK: calculate user location
	func getUserLocation()
	{
		// set loading message
		self.setLoading(message: "Calculating Your Location")
		
		// calculate user location
		self.locationService = LocationService(manager: CLLocationManager(), completion:
		{
			coords, error in
			
			// handle errors
			guard let userLocation = coords else
			{
				if let errorMessage = error
				{
					self.errorMessage(title: "Location Error", message: errorMessage)
				}
				else
				{
					self.errorMessage(title: "Location Error", message: "Error loading user coordinates")
				}
				
				return
			}
			
			// get weather for user
			self.getUserWeather(coords: userLocation)
		})
	}
	
	// MARK: fetch weather information
	func getUserWeather(coords: CLLocationCoordinate2D)
	{
		// update loading message
		self.setLoading(message: "Loading Weather Information")
		
		// check user weather
		self.networkManager = NetworkManager()
		self.networkService = NetworkService()
		
		let url = self.networkService.getURL(latitude: coords.latitude, longitude: coords.longitude)
		
		self.networkService.fetchRemote(url: url, manager: self.networkManager, completion:
		{
			(data, error) in
			
			// handle errors
			guard let result = data else
			{
				if let errorMessage = error
				{
					self.errorMessage(title: "Network Error", message: errorMessage)
				}
				else
				{
					self.errorMessage(title: "Network Error", message: "Error with network connection")
				}
				
				return
			}
			
			// display weather
			self.showWeather(data: result)
		})
	}
	
	// MARK: show weather information
	func showWeather(data: Any)
	{
		// parse JSON into forecast model
		guard let forecast = WeatherAPI.parseJSON(data: data) else
		{
			self.errorMessage(title: "Weather Error", message: "Error reading weather information")
			
			return
		}
		
		// format date
		let formatter = DateFormatter()
		formatter.dateFormat = "EEEE, d MMMM yyyy"
		
		// populate fields
		self.todaysDate.text = formatter.string(from: forecast.date!)
		self.currentLocation.text = forecast.location!
		self.maxTemp.text = "max \(forecast.maxTemp!)\u{00B0}C"
		self.minTemp.text = "min \(forecast.minTemp!)\u{00B0}C"
		
		// display icon
		self.networkImageService = NetworkImageService()
		self.networkImageManager = NetworkImageManager()
		
		let url = networkImageService.getImageURL(icon: forecast.icon!)
		
		networkImageService.fetchRemoteImage(url: url, manager: networkImageManager, completion:
		{
			image in
			
			self.weatherIcon.image = image.scaleImage(toSize: CGSize(width: 30, height: 30))
		})
		
		// set display
		self.setLoading(message: "")
	}
	
	// MARK: show loading messages
	func setLoading(message: String)
	{
		if (message.isEmpty)
		{
			self.detailsView?.isHidden = false
			self.loadingView?.isHidden = true
			
			self.activityIndicator.stopAnimating()
		}
		else
		{
			self.detailsView?.isHidden = true
			self.loadingView?.isHidden = false
			
			self.activityIndicator?.startAnimating()
			self.loadingMessage?.text = message
		}
	}
	
	// MARK: show error messages
	func errorMessage(title: String, message: String)
	{
		self.alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
		self.alert?.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
		self.present(self.alert!, animated: true, completion: nil)
	}

	// MARK: refresh user location
	@IBAction func refreshLocation(_ sender: UIButton)
	{
		self.locationService.start()
	}
	
}
