//
//  Image+scaleImage.swift
//  Weather
//
//  Created by Victor Meyer on 2018/03/06.
//  Copyright © 2018 DVT. All rights reserved.
//

import UIKit

// MARK: scale image
extension UIImage
{
	
	func scaleImage(toSize newSize: CGSize) -> UIImage?
	{
		let newRect = CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height).integral
		UIGraphicsBeginImageContextWithOptions(newSize, false, 0)
		
		if let context = UIGraphicsGetCurrentContext()
		{
			context.interpolationQuality = .high
			let flipVertical = CGAffineTransform(a: 1, b: 0, c: 0, d: -1, tx: 0, ty: newSize.height)
			context.concatenate(flipVertical)
			context.draw(self.cgImage!, in: newRect)
			let newImage = UIImage(cgImage: context.makeImage()!)
			UIGraphicsEndImageContext()
			return newImage
		}
		
		return nil
	}
	
}
