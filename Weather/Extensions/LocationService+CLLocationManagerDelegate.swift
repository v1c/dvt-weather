//
//  LocationService+CLLocationManagerDelegate.swift
//  Weather
//
//  Created by Victor Meyer on 2018/03/05.
//  Copyright © 2018 DVT. All rights reserved.
//

import Foundation
import CoreLocation

// MARK: CLLocationManagerDelegate
extension LocationService: CLLocationManagerDelegate
{
	
	func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation])
	{
		if let userLocation = locations.last?.coordinate
		{
			// save location result
			self.coordinates = userLocation
			self.error = nil
			self.locationResult()
		}
	}
	
	func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus)
	{
		// handle problem responses from location manager
		if (status == .notDetermined)
		{
			self.locationManager?.requestWhenInUseAuthorization()
		}
		
		if (status == .denied)
		{
			// save error
			self.coordinates = nil
			self.error = "Please ensure access to location services is enabled."
			self.locationResult()
		}
	}
	
}
