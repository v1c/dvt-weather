//
//  NetworkImageManager.swift
//  Weather
//
//  Created by Victor Meyer on 2018/03/06.
//  Copyright © 2018 DVT. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireImage

class NetworkImageManager: NetworkImageDelegate
{
	
	// MARK: remote network call
	func fetchRemoteImage(url: URL, completion: @escaping (UIImage) -> Void)
	{
		// run as background thread
		let queue = DispatchQueue(label: "com.dvt.background.image", qos: .background, attributes: [.concurrent])
		
		Alamofire.request(url).responseImage(imageScale: 2.0, inflateResponseImage: true, queue: queue, completionHandler:
		{
			response in
			
			// return to main thread
			DispatchQueue.main.async
			{
				if let remoteImage = response.result.value
				{
					completion(remoteImage)
				}
			}
		})
	}
	
}
