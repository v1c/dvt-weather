//
//  NetworkManager.swift
//  Weather
//
//  Created by Victor Meyer on 2018/03/05.
//  Copyright © 2018 DVT. All rights reserved.
//

import Foundation
import Alamofire

class NetworkManager: NetworkDelegate
{

	// MARK: remote network call
	func fetchRemoteData(url: URL, completion: @escaping (Any?, String?) -> Void)
	{
		// run as background thread
		let queue = DispatchQueue(label: "com.dvt.background", qos: .background, attributes: [.concurrent])
		
		Alamofire.request(url)
		.validate()
		.responseJSON(queue: queue, options: JSONSerialization.ReadingOptions.allowFragments, completionHandler:
		{
			(response) -> Void in
			
			// send responses to main thread
			DispatchQueue.main.async
			{
				guard let data = response.result.value else
				{
					if let error = response.error
					{
						completion(nil, error.localizedDescription)
					}
					else
					{
						completion(nil, "Error with network connection")
					}
					
					return
				}
			
				completion(data, nil)
			}
		})
	}
	
}
