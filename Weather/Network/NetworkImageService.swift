//
//  NetworkImageService.swift
//  Weather
//
//  Created by Victor Meyer on 2018/03/05.
//  Copyright © 2018 DVT. All rights reserved.
//

import UIKit

class NetworkImageService
{
	
	// MARK: properties
	var networkImageManager: NetworkImageDelegate!
	
	// MARK: build image url
	func getImageURL(icon: String) -> URL
	{
		return URL(string: "https://openweathermap.org/img/w/\(icon).png")!
	}
	
	// MARK: retrieve remote image
	func fetchRemoteImage(url: URL, manager: NetworkImageDelegate, completion: @escaping (UIImage) -> ())
	{
		self.networkImageManager = manager
		self.networkImageManager.fetchRemoteImage(url: url, completion: completion)
	}
	
}
