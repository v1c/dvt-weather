//
//  Network.swift
//  Weather
//
//  Created by Victor Meyer on 2018/03/04.
//  Copyright © 2018 DVT. All rights reserved.
//

import Foundation

class NetworkService
{
	
	// MARK: properties
	let APIKey = "72e997b30081e2a7caebe6887926484e"
	let baseURLCurrent = "https://api.openweathermap.org/data/2.5/weather?"
	var networkManager: NetworkDelegate!
	
	// MARK: url builder
	func getURL(latitude: Double, longitude: Double) -> URL
	{
		return URL(string: baseURLCurrent + "lat=\(latitude)" + "&lon=\(longitude)" + "&units=metric" + "&APPID=\(self.APIKey)")!
	}
	
	// MARK: manage remote call
	func fetchRemote(url: URL, manager: NetworkDelegate, completion: @escaping (Any?, String?) -> Void)
	{
		self.networkManager = manager
		self.networkManager.fetchRemoteData(url: url, completion: completion)
	}
	
}
