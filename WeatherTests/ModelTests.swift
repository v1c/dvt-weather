//
//  ModelTests.swift
//  Weather
//
//  Created by Victor Meyer on 2018/03/05.
//  Copyright © 2018 DVT. All rights reserved.
//

import XCTest

class ModelTests: XCTestCase
{

	// MARK: test
	let mockDate = Date()
	let mockLocation = "New York, USA"
	let mockMaxtemp = 30
	let mockMinTemp = 12
	let mockIcon = "01d"
	
	// MARK: setup
    override func setUp()
	{
        super.setUp()
    }
    
    override func tearDown()
	{
        super.tearDown()
    }
	
	// MARK: test if values assigned
    func testForecastSave()
	{
		// given/when
		let forecast = Forecast(date: mockDate, location: mockLocation, maxTemp: mockMaxtemp, minTemp: mockMinTemp, icon: mockIcon)
		
		// then
		XCTAssertEqual(forecast.date, mockDate)
		XCTAssertEqual(forecast.location, mockLocation)
		XCTAssertEqual(forecast.maxTemp, mockMaxtemp)
		XCTAssertEqual(forecast.minTemp, mockMinTemp)
		XCTAssertEqual(forecast.icon, mockIcon)
    }
	
	func testWeatherAPIParsing()
	{
		// given
		guard let pathString = Bundle(for: type(of: self)).path(forResource: "weather", ofType: "json") else {
			fatalError("weather.json not found")
		}
		
		guard let jsonString = try? NSString(contentsOfFile: pathString, encoding: String.Encoding.utf8.rawValue) else {
			fatalError("Unable to convert weather.json to String")
		}
		
		guard let jsonData = jsonString.data(using: String.Encoding.utf8.rawValue) else {
			fatalError("Unable to convert weather.json to NSData")
		}
		
		guard let jsonDictionary = try? JSONSerialization.jsonObject(with: jsonData, options: []) as? [String:AnyObject] else {
			fatalError("Unable to convert UnitTestData.json to JSON dictionary")
		}
		
		// when
		if let forecast = WeatherAPI.parseJSON(data: jsonDictionary!)
		{
			// then
			XCTAssertEqual(forecast.maxTemp, 29)
			XCTAssertEqual(forecast.minTemp, 16)
		}
		else
		{
			XCTFail("forecast creation failed")
		}
	}
	
}
