//
//  NetworkServiceTests.swift
//  Weather
//
//  Created by Victor Meyer on 2018/03/05.
//  Copyright © 2018 DVT. All rights reserved.
//

import XCTest
import CoreLocation

class NetworkTests: XCTestCase
{
	
	// MARK: test expected
	static let mockCoords = CLLocationCoordinate2D(latitude: 10.0, longitude: 20.0)
	static let mockData = Data()
	static let mockError = "Error with Network"
	
	// MARK: properties
	var networkService: NetworkService!
	var successExpectation: XCTestExpectation!
	var failureExpectation: XCTestExpectation!
	var data: Data!
	var error: String!
	
	// MARK: life cycle
    override func setUp()
	{
        super.setUp()

		self.networkService = NetworkService()
		XCTAssertNotNil(self.networkService)
    }
    
    override func tearDown()
	{
		self.networkService = nil
		
        super.tearDown()
    }
	
	// save response and trigger expectations
	func networkServiceStart(manager: NetworkDelegate)
	{
		// given
		self.networkService.fetchRemote(url: URL(string: "https://www.google.com")!, manager: manager, completion:
		{
			(data, error) in
			
			if let _ = data
			{
				self.data = data as? Data
				self.error = nil
				self.successExpectation.fulfill()
			}
			
			if let _ = error
			{
				self.data = nil
				self.error = error
				self.failureExpectation.fulfill()
			}
		})
	}
	
	// MARK: test network responses
    func testNetworkSuccess()
	{
		// when
		self.successExpectation = expectation(description: "called on successful result")
		self.networkServiceStart(manager: NetworkManagerSuccessMock())
		
		waitForExpectations(timeout: 3)
		{
			error in
			
			if let error = error {
				XCTFail("waitForExpectationsWithTimeout error - \(error)")
			}
			
			// then
			XCTAssertEqual(self.data, NetworkTests.mockData)
		}
    }
	
	func testNetworkFailure()
	{
		// when
		self.failureExpectation = expectation(description: "called on error")
		self.networkServiceStart(manager: NetworkManagerFailureMock())

		waitForExpectations(timeout: 3)
		{
			error in
			
			if let error = error {
				XCTFail("waitForExpectationsWithTimeout error - \(error)")
			}
			
			// then
			XCTAssertEqual(self.error, NetworkTests.mockError)
		}
	}
	
}

// MARK: mocked network managers - when
class NetworkManagerSuccessMock: NetworkDelegate
{
	
	func fetchRemoteData(url: URL, completion: @escaping (Any?, String?) -> Void)
	{
		// send data
		completion(NetworkTests.mockData, nil)
	}
	
}

class NetworkManagerFailureMock: NetworkDelegate
{
	
	func fetchRemoteData(url: URL, completion: @escaping (Any?, String?) -> Void)
	{
		// send error
		completion(nil, NetworkTests.mockError)
	}
	
}
