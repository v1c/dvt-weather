//
//  LocationServicesTest.swift
//  Weather
//
//  Created by Victor Meyer on 2018/03/01.
//  Copyright © 2018 DVT. All rights reserved.
//

import XCTest
import CoreLocation

// MARK: main test class for setting up and running tests
class LocationTests: XCTestCase
{
	
	// MARK: test expected
	static let mockLatitude = 20.1
	static let mockLongitude = 30.1
	static let mockError = "Error with Location"
	
	// MARK: properties
	var locationService: LocationService!
	var successExpectation: XCTestExpectation!
	var failureExpectation: XCTestExpectation!
	var coord: CLLocationCoordinate2D!
	var error: String!
	
	// MARK: life cycle
    override func setUp()
	{
        super.setUp()
		
		self.locationService = LocationService(manager: CLLocationManager(), completion:
		{
			coords, error in
			
			// when
			if let _ = coords
			{
				self.coord = coords
				self.successExpectation.fulfill()
			}
			
			if let _ = error
			{
				self.error = error
				self.failureExpectation.fulfill()
			}
		})
		XCTAssertNotNil(self.locationService)
    }
    
    override func tearDown()
	{
		self.locationService = nil
		
        super.tearDown()
    }
	
	// MARK: test location responses
    func testLocationSuccess()
	{
		// given
		self.successExpectation = expectation(description: "called on successful result")
		self.locationService.coordinates = CLLocationCoordinate2D(latitude: LocationTests.mockLatitude, longitude: LocationTests.mockLongitude)
		self.locationService.error = nil
		self.locationService.locationResult()
		
		waitForExpectations(timeout: 3)
		{
			error in
			
			if let error = error {
				XCTFail("waitForExpectationsWithTimeout error - \(error)")
			}
			
			// then
			XCTAssertEqual(self.coord.latitude, LocationTests.mockLatitude)
			XCTAssertEqual(self.coord.longitude, LocationTests.mockLongitude)
		}
    }
	
	func testLocationFailure()
	{
		// given
		self.failureExpectation = expectation(description: "called on error")
		self.locationService.coordinates = nil
		self.locationService.error = LocationTests.mockError
		self.locationService.locationResult()
		
		waitForExpectations(timeout: 3)
		{
			error in
			
			if let error = error {
				XCTFail("waitForExpectationsWithTimeout error - \(error)")
			}
			
			// then
			XCTAssertEqual(self.error, LocationTests.mockError)
		}
	}
    
}
